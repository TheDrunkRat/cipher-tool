#!/usr/bin/env python

import kivy
from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
from cipher import Cipher

kivy.require('1.7.0')


class MainLayout(FloatLayout):
    key = 0
    decode = True
    cipher = Cipher(key, 'left')

    def on_text(self, instance):
        pass

    def increase_key(self):
        self.key += 1
        self.ids.w_key.text = str(self.key)
        self.cipher.key = self.key
        self.encode()

    def decrease_key(self):
        if self.key == 0:
            return
        else:
            self.key -= 1
            self.ids.w_key.text = str(self.key)
            self.cipher.key = self.key
        self.encode()

    def encode(self): # and decode
        if self.decode == False:
            text = str(self.ids.w_text.text)
            self.ids.w_result.text = self.cipher.encode(text)
        else:
            text = str(self.ids.w_text.text)
            self.ids.w_result.text = self.cipher.decode(text)

    def change_shift(self):
        if self.ids.w_shift.text == 'left':
            self.ids.w_shift.text = 'right'
            self.cipher.shift = 'right'
        else:
            self.ids.w_shift.text = 'left'
            self.cipher.shift = 'left'
        self.encode()
    
    def change_operation(self):
        if self.decode == False:
            self.ids.w_operation.text = 'decoder'
            self.decode = True
        else:
            self.ids.w_operation.text = 'encoder'
            self.cipher.operation = 'encoder'
            self.decode = False
        self.encode()


class CipherToolApp(App):
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    CipherToolApp().run()
