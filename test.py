import unittest
from cipher import Cipher

class CipherTests(unittest.TestCase):

    def test_encode_left(self):
        c = Cipher(5, 'left')
        open_text = "This is a test"
        coded_text = c.encode(open_text)
        self.assertEqual(coded_text, "OCDN DN V OZNO")

    def test_encode_right(self):
        c = Cipher(5, 'right')
        open_text = "This is a test"
        coded_text = c.encode(open_text)
        self.assertEqual(coded_text, "YMNX NX F YJXY")

    def test_decode_left(self):
        c = Cipher(5, 'left')
        coded_text = "OCDN DN V OZNO"
        open_text = c.decode(coded_text)
        self.assertEqual(open_text, "THIS IS A TEST")

    def test_decode_right(self):
        c = Cipher(5, 'right')
        coded_text = "YMNX NX F YJXY"
        open_text = c.decode(coded_text)
        self.assertEqual(open_text, "THIS IS A TEST")

    def test_encode_numbers(self):
        c = Cipher(5, 'right')
        open_text = "123 four 5"
        coded_text = c.encode(open_text)
        self.assertEqual(coded_text, "123 KTZW 5")
    
    def test_decode_numbers(self):
        c = Cipher(5, 'right')
        coded_text = "123 KTZW 5"
        open_text = c.decode(coded_text)
        self.assertEqual(open_text, "123 FOUR 5")

    def test_encode_special_characters(self):
        c = Cipher(5, 'right')
        open_text = "1.3 + 1,4 = 2.7;"
        coded_text = c.encode(open_text)
        self.assertEqual(coded_text, "1.3 + 1,4 = 2.7;")
    
    def test_decode_special_characters(self):
        c = Cipher(5, 'right')
        coded_text = "1.3 + 1,4 = 2.7;"
        open_text = c.decode(coded_text)
        self.assertEqual(open_text, "1.3 + 1,4 = 2.7;")

if __name__ == '__main__':
    unittest.main()
