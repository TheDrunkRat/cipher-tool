# Cipher Tool
Author: Tomislav Kaučić
## Description
Cipher tool is a program used for encoding and decoding strings by using Caesar Cipher method.

## Functions

### Constructor

Parameters:
- `key` - integer; used to specify how many times should alphabet shift. Default value is 0.
- `shift` - string; defines the direction of shifting. Default value is "left"

### Other Functions

`encode(text)` - encodes text and returns encoded text

`decode(text)` - decodes encoded text and returns open text

## Tests

### Test 1
**TEST FUNCTION NAME:** `test_encode_left()`

**TEST DESCRIPTION:** Check does cipher encode correctly when shifted left. Open text is "This is a test", key is set to 5 and result should be "OCDN DN V OZNO".

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `left`.
2. Create new variable open_text and set it to "This is a test".
3. Create new variable coded_text and set it to result of `encode(open_text)`.

### Test 2
**TEST FUNCTION NAME:** `test_encode_right()`

**TEST DESCRIPTION:** Check does cipher encode correctly when shifted right. Open text is "This is a test", key is set to 5 and result should be "YMNX NX F YJXY".

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable open_text and set it to "This is a test".
3. Create new variable coded_text and set it to result of `encode(open_text)`.

### Test 3
**TEST FUNCTION NAME:** `test_decode_left()`

**TEST DESCRIPTION:** Check does cipher decode correctly when shifted left. Coded text is "OCDN DN V OZNO", key is set to 5 and result should be "THIS IS A TEST".

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `left`.
2. Create new variable coded_text and set it to "OCDN DN V OZNO".
3. Create new variable open_text and set it to result of `decode(coded_text)`.

### Test 4
**TEST FUNCTION NAME:** `test_decode_right()`

**TEST DESCRIPTION:** Check does cipher decode correctly when shifted right. Coded text is "YMNX NX F YJXY", key is set to 5 and result should be "THIS IS A TEST".

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable coded_text and set it to "YMNX NX F YJXY".
3. Create new variable open_text and set it to result of `decode(coded_text)`.

### Test 5
**TEST FUNCTION NAME:** `test_encode_numbers()`

**TEST DESCRIPTION:** Check does cipher encode numbers correctly (by default, numbers should not be encoded, they should be same in coded text as in the open text).

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable open_text and set it to "123 four 5".
3. Create new variable coded_text and set it to result of `encode(open_text)`.

### Test 6
**TEST FUNCTION NAME:** `test_decode_numbers()`

**TEST DESCRIPTION:** Check does cipher decode numbers correctly (by default, numbers should not be decoded, they should be same in coded text as in the open text).

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable coded_text and set it to "123 KTZW 5".
3. Create new variable open_text and set it to result of `decode(coded_text)`.

### Test 7
**TEST FUNCTION NAME:** `test_encode_special_characters()`

**TEST DESCRIPTION:** Check does cipher decode special characters(. , + - = etc.) correctly (by default, special characters should not be encoded, they should be same in coded text as in the open text).

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable open_text and set it to "1.3 + 1,4 = 2.7;".
3. Create new variable coded_text and set it to result of `encode(open_text)`.

### Test 8
**TEST FUNCTION NAME:** `test_decode_special_characters()`

**TEST DESCRIPTION:** Check does cipher decode special characters(. , + - = etc.) correctly (by default, special characters should not be encoded, they should be same in coded text as in the open text).

**IMPLEMENTATION STEPS:**
1. Create new Cipher object with key of 5 and shift set to `right`.
2. Create new variable coded_text and set it to "1.3 + 1,4 = 2.7;".
3. Create new variable open_text and set it to result of `decode(coded_text)`.