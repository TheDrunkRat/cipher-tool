import string

class Cipher(object):
    key = 0
    shift = 'left'
    alphabet = list(string.ascii_uppercase)

    def __init__(self, key, shift):
        self.key = key
        self.shift = shift

    def encode(self, text):
        """Encodes open text and returns encoded text.

        :param text: open text (information)
        :returns: string
        """
        __input = list(text.upper())
        __result = []
        for letter in __input:
            if (not letter.isalpha()):
                __result.append(letter)
                continue
            alphabet_index = self.alphabet.index(letter)
            if (self.shift == 'left'):
                coded_letter = self.alphabet[(
                    alphabet_index-self.key) % len(self.alphabet)]
                __result.append(coded_letter)
            elif (self.shift == 'right'):
                coded_letter = self.alphabet[(
                    alphabet_index+self.key) % len(self.alphabet)]
                __result.append(coded_letter)
        return ''.join(__result)

    def decode(self, text):
        """Decodes encoded text and returns open text.
        
        :param text: encoded text
        :returns: string
        """
        __input = list(text.upper())
        __result = []
        for letter in __input:
            if (not letter.isalpha()):
                __result.append(letter)
                continue
            alphabet_index = self.alphabet.index(letter)
            if (self.shift == 'left'):
                decoded_letter = self.alphabet[(
                    alphabet_index+self.key) % len(self.alphabet)]
                __result.append(decoded_letter)
            elif (self.shift == 'right'):
                decoded_letter = self.alphabet[(
                    alphabet_index-self.key) % len(self.alphabet)]
                __result.append(decoded_letter)
        return ''.join(__result)


if __name__ == "__main__":
    c = Cipher(23, 'right')
    print(c.encode("ABCD"))
    print(c.decode("XYZA"))